import requests as r
from datetime import datetime

def obtain_csv():

    print "obtaining csv !!"
    url = ['bigrock.in']#, 'cloudflare.com']
    for _url in url:

        e_url = 'http://www.dailychanges.com/export/'+_url+'/'+datetime.utcnow().strftime("%Y-%m-%d")+'/export.csv'
        s = r.Session()

        print "getting %s" % e_url
        s.get("http://www.dailychanges.com/")

        s.headers.update({
            'Host': 'www.dailychanges.com', 
            'Referer':"http://www.dailychanges.com/"+_url, 
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, sdch',
            'Accept-Language': 'en-GB,en;q=0.8,en-US;q=0.6,hi;q=0.4'
        })

        csvout = s.get(e_url)
        print csvout.text

obtain_csv()
