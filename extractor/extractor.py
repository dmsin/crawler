"""
Author: Nairitya Khilari
Date: 23 Dec 2014
DMS Infosystem
"""

from db import db
import config
import requests as r
from datetime import datetime
import csv, sys
import json, jsontree

class Extractor:

    def __init__(self):

        self.url = 'http://www.dailychanges.com/'#export/'#+datetime.utcnow().strftime("%Y-%m-%d")+'/export.csv'
        configuration = config.config()
        con = db.Database(configuration)
        con.connect()

        con.create_table_site()
        con.create_table_info()
        self.con = con
        self.username = ['dmsinfosystem2010', 'dmsinfosystem2011', 'dmsinfosystem2012', 'dmsinfosystem2013', 'dmsinfosystem2014', 'dmsinfosystem2015']

        self.log_file = "log/log.log"
        self.count = int(self.read_file())
        self.all_data = self.con.select_new_from_site(self.count)

    def start(self, index):

        print "Starting this !!"
        self.obtain_csv()

        print "Loggin in !!"
        self.login_in_whois(index)

        print "Starting extration of data !!"
        self.extract_whois_data(index)

    def obtain_csv(self):

        today_date = datetime.utcnow().strftime("%Y-%m-%d")
        self.con.delete_site()

        url = ['bigrock.in', 'cloudflare.com']
        for _url in url:

            e_url = self.url+'export/'+_url+'/2014-12-24/export.csv'
            s = r.Session()

            s.get(self.url)

            s.headers.update({
                'Host': 'www.dailychanges.com', 
                'Referer': self.url+_url, 
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate, sdch',
                'Accept-Language': 'en-GB,en;q=0.8,en-US;q=0.6,hi;q=0.4'
            })

            csvout = s.get(e_url)

            f = open("export.csv", "w")
            f.write(csvout.text)
            f.close()
            
            with open('export.csv', 'rb') as f:
                reader = csv.reader(f)
                i = 0
                for row in reader:
                    if i > 2:
                        try:
                            self.con.insert_in_site(row[0], row[1])
                            # pass
                        except Exception, e:
                            print str(e)
                            pass
                        
                    i+=1
            f.close()

        self.all_data = self.con.select_new_from_site(self.count)

    def login_in_whois(self, index):

        login_url = "https://www.whoisxmlapi.com/processlogin.php"

        s = r.Session()
        s.get("https://www.whoisxmlapi.com/")

        index = int(index)
        if(index >= len(self.username)):
            print "All accounts exhausted !!"
            sys.exit()

        data = {
        "username": self.username[index],
        "password": self.username[index],
        "returnto": "/index.php",
        "go":""
        }

        s.headers.update({
            "Host":"www.whoisxmlapi.com",
            "Origin":"https://www.whoisxmlapi.com",
            "Referer":"https://www.whoisxmlapi.com/index.php",
            "User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36"
        })

        print "Logging in using username %s" % self.username[index]

        try:
            s.post(login_url, data=data, timeout=7)
            self.hero = s
            print "Logged in succesfully !!"

        except Exception, e:
            print "It seems internet is down !!, try me later !!"
            sys.exit()

    def extract_whois_data(self, index):
        
        for _all_data in self.all_data:

            use_data = {
                "domainName": _all_data,
                "outputFormat" : "json",
                "userName": self.username[index],
                "password": self.username[index]
            }

            url = "http://www.whoisxmlapi.com/whoisserver/WhoisService"

            try:
                p = self.hero.get(url=url, params=use_data, timeout=7)

                try:
                    check_it = json.loads(p.text)["ErrorMessage"]
                    #This means limit 500 has been reached !!, 
                    print "Limit of 500 has been reached !!"
                    index += 1
                    self.logout_whois()
                    self.translate_index(index)

                except Exception, e:
                    self.count += 1
                    self.manage(p.text, int(self.count), _all_data)

            except r.exceptions.RequestException:
                print "Slow internet !! skipping for this %s" % _all_data
                print "Resuming...."

        print "I am done !! :)"

    def translate_index(self, index):

        if(index >= len(self.username)):
            print "All username's exhausted !! add more accounts to proceed !!"
            self.crash_now()

        else:
            self.login_in_whois(index)

    def logout_whois(self):

        print "logging out!!"
        logout_url = "https://www.whoisxmlapi.com/logout.php"
        params = {
            "returnto": "/index.php"
        }

        self.hero.get(logout_url, params = params)


    def manage(self, json_text, count, _all_data):
        data = jsontree.jsontree

        try:
            parsed_json = json.loads(json_text)["WhoisRecord"]["administrativeContact"]
            data.name = parsed_json["name"]
            data.organization = parsed_json["organization"]
            data.email = parsed_json["email"]
            data.telephone = parsed_json["telephone"]
            data.address = parsed_json["street1"] + ", " +parsed_json["city"]
            data.site = _all_data;
            
            print "inserting count %s" % count
            self.con.insert_in_info(data, count)

        except KeyError:
            print "Record Doesn't exist"
            pass


    #Get all data from database !!
    def getall(self):
        return str(self.con.getall())


    #read the recent count !!
    def read_file(self):

        f = open(self.log_file, 'r')
        count = f.read()
        f.close()

        if(count == ''):
            return 0

        try:
            return int(count)
        except Exception, e:
            print "There is some random text in my log file !!"
            print "Don't mess with me!"
            print "I am exiting !!"
            sys.exit()

    #Let us resume the flow !!
    def crash_now(self):

        f = open(self.log_file, 'w')
        f.write(str(self.count))
        f.close()

        print "Wrote the offset !!"
