from setuptools import setup

setup(name='dmspython',
      version='2.0',
      description='A Demo app',
      author='Nairitya Khilari',
      author_email='nkmann2@gmail.com',
      url='http://www.python.org/sigs/distutils-sig/',
     install_requires=['wsgiref', 'jsontree', 'psycopg2', 'requests', 'argparse'],
)
