"""
Author: Nairitya Khilari
Date: 23 Dec 2014
DMS Infosystem
"""

import os, sys
import psycopg2
import json
from datetime import datetime

class Database:

    def __init__(self, config):

        self.host = config['host']
        self.user = config['username']
        self.password = config['password']
        self.database = config['db']

    def connect(self):

        connection = psycopg2.connect(
            host=self.host, 
            user=self.user, 
            password=self.password, 
            database=self.database
        )
        self.connection = connection
        sys.stdout.write("Connected to db!!")
        return connection

    def reset_connection(self):
        sys.stdout.write("Closing the connection.\n")
        self.connection.close()
        sys.stdout.write("Connection closed\nStarting connection to database\n")
        self.connect()
        sys.stdout.write("Connection has been established.\n")


    def close_connection(self):
        self.connection.close()


    def create_table_site(self):

        query = """
            CREATE TABLE IF NOT EXISTS site (
                id bigserial primary key,
                site varchar(200) default NULL,
                type varchar(20) default NULL,
                domain varchar(100) NOT NULL,
                date_added varchar(50) default NULL
            )
        """

        cursor = self.connection.cursor()
        conn = self.connection

        try:
            cursor.execute(query)
            conn.commit()
            sys.stdout.write("created table site\n")
            cursor.close()

        except Exception, e:
            self.reset_connection()
            sys.stdout.write(str(e))


    def insert_in_site(self, site, type_, _all_data):

        date_added = datetime.utcnow().strftime("%Y-%m-%d")

        query = """
            INSERT INTO site (
                site, type, date_added, domain
            ) VALUES (\'%s\', \'%s\', \'%s\', \'%s\')
        """ % (site, type_, date_added, _all_data)

        cursor = self.connection.cursor()
        conn = self.connection

        try:
            cursor.execute(query)
            conn.commit()
            print "Inserted %s and %s in table site" % (site, type_)
            cursor.close()

        except Exception, e:
            self.reset_connection()
            sys.stdout.write(str(e))

    def create_table_info(self):

        query = """
            CREATE TABLE IF NOT EXISTS info (
                id bigserial primary key,
                name varchar(100) default NULL,
                organization varchar(100) default NULL,
                date_added varchar(50) default NULL,
                email varchar(100) default NULL,
                telephone varchar(100) default NULL,
                address varchar(400) default NULL,
                count integer
            )
        """

        cursor = self.connection.cursor()
        conn = self.connection

        try:
            cursor.execute(query)
            conn.commit()
            sys.stdout.write("created table info\n")
            cursor.close()

        except Exception, e:
            self.reset_connection()
            sys.stdout.write(str(e))

    def select_new_from_site(self, offset=0):

        date_added = datetime.utcnow().strftime("%Y-%m-%d")
        query = """
            SELECT site from site WHERE type='new' AND date_added = \'%s\' offset \'%s\'
        """ % (date_added, offset)

        cursor = self.connection.cursor()

        try:
            cursor.execute(query)
            result = cursor.fetchall()
            cursor.close()

        except Exception, e:
            sys.stdout.write(str(e))

        return result

    def insert_in_info(self, data, count):

        name = data.name
        organization = data.organization
        email = data.email
        telephone = data.telephone
        address = data.address

        date_added = datetime.utcnow().strftime("%Y-%m-%d")
        query = """
            INSERT INTO info 
            (
                name, organization, email, 
                telephone, address, date_added, 
                count) VALUES (\'%s\', \'%s\', \'%s\', 
                \'%s\', \'%s\', \'%s\', \'%s\'
            )
        """ % (name, organization, email, telephone, address, date_added, count)

        cursor = self.connection.cursor()
        conn = self.connection

        try:
            cursor.execute(query)
            conn.commit()
            print "Inserted data %s\n" % name
            cursor.close()

        except Exception, e:
            self.reset_connection()
            sys.stdout.write(str(e))

    def getall(self):

        date_added = datetime.utcnow().strftime("%Y-%m-%d")

        query = """
            SELECT * FROM info WHERE date_added = \'%s\'
            """ % date_added

        cursor = self.connection.cursor()

        try:
            cursor.execute(query)
            result = cursor.fetchall()
            cursor.close()

        except Exception, e:
            self.reset_connection()
            sys.stdout.write(str(e))

        return result

    def delete_site(self):

        query = """
            DELETE FROM site WHERE date_added = \'%s\'
        """ % datetime.utcnow().strftime("%Y-%m-%d")

        cursor = self.connection.cursor()

        try:
            cursor.execute(query)
            cursor.close()

        except Exception, e:
            self.reset_connection()
            sys.stdout.write(str(e))

        print "Deleted entries from table site !!"